#!/bin/sh

output=$(yard stats --list-undoc --verbose --compact)

echo "$output"

if (echo $output | grep -q 'Undocumented Objects'); then exit 1; fi
if (echo $output | grep -q '\[warn\]'); then exit 1; fi
if (echo $output | grep -q '\[error\]'); then exit 1; fi
