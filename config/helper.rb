# frozen_string_literal: true

require_relative '../lib/innohack'
require_relative 'initializers/logger'
require_relative 'initializers/mongoid'
require_relative 'initializers/models'
require_relative 'initializers/json'
require_relative 'initializers/api'
