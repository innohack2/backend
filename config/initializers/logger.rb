# frozen_string_literal: true

# Инициализация журнала событий

require 'logger'

# Отключение буферизации стандартного потока вывода
$stdout.sync = true

# Журнал событий
LOG = Logger.new($stdout)
LOG.level = ENV['LOG_LEVEL'] || Logger::DEBUG
LOG.formatter = lambda do |severity, datetime, _progname, message|
  "[#{$PROGRAM_NAME}] [#{datetime.strftime('%F %T')}] #{severity}: #{message}\n"
end
