# frozen_string_literal: true

# Настройки контроллера

Innohack.equip('api/controller')
Innohack.equip('api')

Innohack::API::Controller.configure do |settings|
  settings.set :bind, ENV['BIND'] || '0.0.0.0'
  settings.set :port, ENV['PORT'] || '8080'
  settings.set :server, :puma
  settings.disable :static
  settings.disable :show_exceptions, :dump_errors
end
