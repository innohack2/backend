# frozen_string_literal: true

require 'mongoid'

Mongoid.load!(File.join(File.absolute_path("#{__dir__}/.."), 'database.yml'))

Mongoid.logger = LOG
Mongo::Logger.logger = Mongoid.logger
