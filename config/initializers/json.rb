# frozen_string_literal: true

# Настройки обработки JSON

require 'oj'

Oj.default_options = { symbol_keys: true, mode: :strict }
