# Псевдонимы основных команд

.PHONY: build-dev launch install debug run review test doc

SHELL = /bin/sh

IMAGE = innohack_backend

build-dev:
	docker build -t $(IMAGE) .

launch:
	docker-compose run --service-ports --name app app; docker-compose down

install:
	bundle install

debug:
	bundle exec irb

run:
	bundle exec ruby app.rb

review:
	rubocop && ./bin/check_docs.sh

test:
	LOG_LEVEL=unknown bundle exec rspec --order rand --fail-fast

doc:
	yardoc --no-stats --no-progress --quiet
