FROM ruby:2.7-alpine3.13 AS app

RUN apk upgrade --update-cache && \
    apk add make tzdata && \
    cp /usr/share/zoneinfo/Europe/Moscow /etc/localtime && \
    echo "Europe/Moscow" > /etc/timezone && \
    apk del tzdata && \
    rm -f /var/cache/apk/*

WORKDIR /usr/src/app

EXPOSE 8080

COPY Gemfile* ./

RUN apk add --no-cache gcc musl-dev && \
    bundle config set without 'development test' && \
    bundle install --jobs=4 --retry=3 && \
    apk del gcc musl-dev

COPY . ./

CMD ["bundle", "exec", "ruby", "app.rb"]

FROM app

RUN bundle config unset 'without' && \
    bundle install --jobs=4 --retry=3

CMD ["sh"]
