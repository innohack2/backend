# frozen_string_literal: true

require 'irb/completion'

require_relative 'config/helper'

begin
  require 'awesome_print'
rescue LoadError
  nil
end

begin
  require 'rspec'
  require_relative 'spec/spec_helper'
rescue LoadError
  nil
end
