# frozen_string_literal: true

module Innohack
  # Пространство моделей
  module Models
    # Модель урока
    # @!attribute [r] id
    #   Идентификатор урока
    #   @return [BSON::ObjectId]
    #     идентификатор урока
    # @!attribute name
    #   Название урока
    #   @return [String]
    #     название урока
    # @!attribute content
    #   Содержимое урока
    #   @return [String]
    #     содержимое урока
    # @!attribute content
    #   Вопросы для тестирования после прохождения урока
    #   @return [Array<Hash>]
    #     вопросы для тестирования после прохождения урока
    # @!attribute course
    #   Курс, в котором находится урок
    #   @return [Innohack::Models::Course]
    #     курс, в котором находится урок
    class Lesson
      include Mongoid::Document

      attr_readonly :id
      field :name, type: String
      field :content, type: String
      embeds_many :questions
      belongs_to :course, class_name: 'Innohack::Models::Course'
    end
  end
end
