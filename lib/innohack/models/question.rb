# frozen_string_literal: true

module Innohack
  # Пространство моделей
  module Models
    # Модель вопроса
    # @!attribute title
    #   Заголовок вопроса
    #   @return [String]
    #     заголовок вопроса
    # @!attribute variants
    #   Варианты ответов
    #   @return [Array<String>]
    #     варианты ответов
    # @!attribute answer
    #   Ответ на вопрос
    #   @return [String]
    #     ответ на вопрос
    # @!attribute lesson
    #   урок, к которому относится вопрос
    #   @return [Innohack::Models::Lesson]
    #     урок, к которому относится вопрос
    class Question
      include Mongoid::Document

      field :title, type: String
      field :variants, type: Array
      field :answer, type: String
      embedded_in :lesson
    end
  end
end
