# frozen_string_literal: true

module Innohack
  # Пространство моделей
  module Models
    # Модель пользователя
    # @!attribute [r] id
    #   Идентификатор пользователя
    #   @return [BSON::ObjectId]
    #     идентификатор пользователя
    # @!attribute name
    #   Имя и фамилия пользователя
    #   @return [String]
    #     имя и фамилия пользователя
    # @!attribute role
    #   Роль пользователя
    #   @return [String]
    #     роль пользователя
    # @!attribute courses
    #   Курсы, которые завершил пользователь
    #   @return [Array<BSON::ObjectId>]
    #     идентификаторы курсов, которые завершил пользователь
    # @!attribute lessons
    #   Уроки, которые завершил пользователь
    #   @return [Array<BSON::ObjectId>]
    #     идентификаторы уроков, которые завершил пользователь
    # @!attribute cookies
    #   Количество печенья
    #   @return [Integer]
    #     количество печенья
    class User
      include Mongoid::Document

      attr_readonly :id
      field :name, type: String
      field :role, type: String
      field :courses, type: Array, default: []
      field :lessons, type: Array, default: []
      field :cookies, type: Integer, default: 0
    end
  end
end
