# frozen_string_literal: true

module Innohack
  # Пространство моделей
  module Models
    # Модель курса
    # @!attribute [r] id
    #   Идентификатор курса
    #   @return [BSON::ObjectId]
    #     идентификатор курса
    # @!attribute name
    #   Название курса
    #   @return [String]
    #     название курса
    # @!attribute description
    #   Описание курса
    #   @return [String]
    #     описание курса
    # @!attribute roles
    #   Роли пользователей, для которых предназначен курс
    #   @return [Array<String>]
    #     роли пользователей, для которых предназначен курс
    # @!attribute lessons
    #   Уроки курса
    #   @return [Array<Innohack::Models::Lesson>]
    #     список уроков курса
    class Course
      include Mongoid::Document

      attr_readonly :id
      field :name, type: String
      field :description, type: String
      field :roles, type: Array, default: []
      has_many :lessons, class_name: 'Innohack::Models::Lesson'
    end
  end
end
