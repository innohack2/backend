# frozen_string_literal: true

module Innohack
  module API
    # Пространство методов REST API, отвечающих за "аутентификацию"
    module Auth
      # Пространство метода REST API, который идентифицирует или регистрирует
      # пользователя
      module Login
        # Идентифицирует или регистрирует пользователя
        # @request_body [JSON]
        #   тело запроса, соответствующее JSON-схеме {REQUEST_BODY_SCHEMA}
        # @status
        #   200 OK
        # @response_body [JSON]
        #   идентификатор пользователя в формате удовлетворяющем JSON-схеме
        #   {RESPONSE_BODY_SCHEMA}
        Controller.post '/login' do
          JSON::Validator.validate!(REQUEST_BODY_SCHEMA, request_body)
          user_data = Oj.load(request_body)
          user = Innohack::Models::User.where(user_data).first
          user ||= Innohack::Models::User.create(user_data)
          body Oj.dump(user.id.to_s)
          content_type :json
          status :ok
        end

        # JSON-схема тела запроса
        REQUEST_BODY_SCHEMA = {
          type: :object,
          properties: {
            name: {
              type: :string
            },
            role: {
              type: :string
            }
          },
          required: %i[
            name
            role
          ]
        }.freeze

        # JSON-схема тела ответа
        RESPONSE_BODY_SCHEMA = {
          type: :string
        }.freeze
      end
    end
  end
end
