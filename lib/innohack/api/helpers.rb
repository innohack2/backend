# frozen_string_literal: true

module Innohack
  module API
    # Модуль, предоставляющий вспомогательные методы для REST API контроллера
    module Helpers
      # Сообщение о несоответствии тела запроса JSON-схеме
      JSON_SCHEMA_VALIDATION_ERROR_MESSAGE =
        'Request body does not match the JSON-schema'

      # Тело запроса
      # @return [String]
      #   тело запроса
      def request_body
        @request_body ||= request.body.rewind && request.body.read
      end

      # Создаёт запись в журнале событий с информацией о входящем запросе
      def log_request
        LOG.info do
          method = request.request_method
          path = request.path_info
          body = request_body
          "REQUEST: `#{method} #{path}` WITH PARAMS #{params} AND BODY #{body}"
        end
      end

      # Создаёт запись в журнале событий с информацией об ответе на запрос
      def log_response
        LOG.info { "RESPONSE: #{response.body} #{response.status}" }
      end

      # Возвращает объект ошибки, возникшей в процессе обработки запроса
      # @return [Exception]
      #   объект ошибки, возникшей в процессе обработки запроса
      def error
        env['sinatra.error']
      end

      # Возвращает местоположение возникновения ошибки
      # @return [String]
      #   местоположение возникновения ошибки
      def error_place
        root = ::Innohack.root
        backtrace = error.backtrace.select { |string| string.start_with?(root) }
        backtrace.map { |string| string.sub("#{root}/", '') }
      end

      # Создаёт запись в журнале событий с информацией об ошибке, возникшей в
      # процессе обработки запроса
      def log_processing_error
        LOG.error { "#{error.class}: #{error.message} AT #{error_place}" }
      end
    end
  end
end
