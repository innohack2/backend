# frozen_string_literal: true

module Innohack
  module API
    # Пространство методов REST API, предоставляющих доступ к урокам
    module Lessons
      # Пространство метода REST API, возвращающего вопросы для тестирования
      module Questions
        # Возвращает вопросы для тестирования
        # @status
        #   200 OK
        # @response_body [JSON]
        #   список вопросов для тестирования, структура которого соответствует
        #   JSON-схеме {RESPONSE_BODY_SCHEMA}
        Controller.get '/lessons/:id/questions' do |id|
          lesson = Innohack::Models::Lesson.find(id)
          data = lesson.questions.map do |question|
            { title: question.title, answers: question.variants }
          end
          body Oj.dump(data)
          content_type :json
          status :ok
        end

        # JSON-схема тела ответа
        RESPONSE_BODY_SCHEMA = {
          type: :array,
          items: {
            type: :object,
            properties: {
              title: {
                type: :string
              },
              answers: {
                type: :array,
                items: {
                  type: :string
                }
              }
            },
            required: %i[
              title
              answers
            ]
          }
        }.freeze
      end
    end
  end
end
