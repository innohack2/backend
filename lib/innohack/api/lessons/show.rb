# frozen_string_literal: true

module Innohack
  module API
    # Пространство методов REST API, предоставляющих доступ к урокам
    module Lessons
      # Пространство метода REST API, возвращающего информацию об уроке
      module Show
        # Возвращает информацию об уроке
        # @status
        #   200 OK
        # @response_body [JSON]
        #   информация об уроке, структура которой соответствует JSON-схеме
        #   {RESPONSE_BODY_SCHEMA}
        Controller.get '/lessons/:id' do |id|
          lesson = Innohack::Models::Lesson.find(id)
          data = { name: lesson.name, content: lesson.content }
          body Oj.dump(data)
          content_type :json
          status :ok
        end

        # JSON-схема тела ответа
        RESPONSE_BODY_SCHEMA = {
          type: :object,
          properties: {
            name: {
              type: :string
            },
            content: {
              type: :string
            }
          },
          required: %i[
            name
            content
          ]
        }.freeze
      end
    end
  end
end
