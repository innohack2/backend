# frozen_string_literal: true

module Innohack
  module API
    # Пространство методов REST API, предоставляющих доступ к курсам
    module Courses
      # Пространство метода REST API, возвращающего информацию о курсе
      module Show
        # Возвращает информацию о курсе
        # @status
        #   200 OK
        # @response_body [JSON]
        #   информация о курсе, структура которой соответствует JSON-схеме
        #   {RESPONSE_BODY_SCHEMA}
        Controller.get '/courses/:id' do |id|
          course = Innohack::Models::Course.find(id)
          user = Innohack::Models::User.find(params[:user_id])
          lessons = course.lessons.to_a.map do |lesson|
            {
              id: lesson.id.to_s,
              name: lesson.name,
              passed: user.lessons.include?(lesson.id)
            }
          end
          data = {
            name: course.name,
            description: course.description,
            lessons: lessons
          }
          body Oj.dump(data)
          content_type :json
          status :ok
        end

        # JSON-схема тела ответа
        RESPONSE_BODY_SCHEMA = {
          type: :object,
          properties: {
            name: {
              type: :string
            },
            description: {
              type: :string
            },
            lessons: {
              type: :array,
              items: {
                type: :object,
                properties: {
                  id: {
                    type: :string
                  },
                  name: {
                    type: :string
                  },
                  passed: {
                    type: :boolean
                  }
                },
                required: %i[
                  id
                  name
                  passed
                ]
              }
            }
          },
          required: %i[
            name
            description
            lessons
          ]
        }.freeze
      end
    end
  end
end
