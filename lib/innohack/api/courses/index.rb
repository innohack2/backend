# frozen_string_literal: true

module Innohack
  module API
    # Пространство методов REST API, предоставляющих доступ к курсам
    module Courses
      # Пространство метода REST API, возвращающего список курсов подобранных
      # под пользователя
      module Index
        # Возвращает список курсов
        # @status
        #   200 OK
        # @response_body [JSON]
        #   список курсов, структура которого соответствует JSON-схеме
        #   {RESPONSE_BODY_SCHEMA}
        Controller.get '/courses' do
          user = Innohack::Models::User.find(params[:user_id])
          data = Innohack::Models::Course.all.to_a.map do |course|
            type = if user.courses.include?(course.id)
                     'passed'
                   else
                     course.roles.include?(user.role) ? 'user' : 'general'
                   end
            all_lessons = course.lessons.to_a
            passed_lessons = all_lessons.select do |lesson|
              user.lessons.include?(lesson.id)
            end
            progress = "#{passed_lessons.count}/#{all_lessons.count}"
            {
              id: course.id.to_s,
              name: course.name,
              type: type,
              progress: progress
            }
          end
          body Oj.dump(data)
          content_type :json
          status :ok
        end

        # JSON-схема тела ответа
        RESPONSE_BODY_SCHEMA = {
          type: :array,
          items: {
            type: :object,
            properties: {
              id: {
                type: :string
              },
              name: {
                type: :string
              },
              type: {
                type: :string,
                enum: %w[user general passed]
              },
              progress: {
                type: :string,
                pattern: '\d+/\d+'
              }
            },
            required: %i[
              id
              name
              type
              progress
            ]
          }
        }.freeze
      end
    end
  end
end
