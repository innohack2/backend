# frozen_string_literal: true

require 'sinatra/base'
require 'json-schema'
require_relative 'helpers'

module Innohack
  # Пространство имён для REST API
  module API
    # Контроллер REST API
    class Controller < Sinatra::Base
      helpers Helpers

      before { log_request }

      after { log_response }

      error JSON::Schema::ValidationError do
        log_processing_error
        status :unprocessable_entity
        body JSON_SCHEMA_VALIDATION_ERROR_MESSAGE
      end

      error Mongoid::Errors::DocumentNotFound do
        log_processing_error
        status :not_found
      end

      error 500 do
        log_processing_error
        body "#{error.class}: #{error.message}"
      end
    end
  end
end
