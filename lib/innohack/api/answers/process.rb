# frozen_string_literal: true

module Innohack
  module API
    # Пространство методов REST API, взаимодействующих с ответами
    module Answers
      # Пространство метода REST API, который обрабатывает ответ пользователя
      module Process
        # Обрабатывает ответ пользователя
        # @request_body [JSON]
        #   тело запроса, соответствующее JSON-схеме {REQUEST_BODY_SCHEMA}
        # @status
        #   200 OK
        # @response_body [JSON]
        #   статус прохождения теста в формате удовлетворяющем JSON-схеме
        #   {RESPONSE_BODY_SCHEMA}
        Controller.post '/answers' do
          JSON::Validator.validate!(REQUEST_BODY_SCHEMA, request_body)
          params = Oj.load(request_body)
          lesson = Innohack::Models::Lesson.find(params[:lesson_id])
          user = Innohack::Models::User.find(params[:user_id])
          user_answers = params[:answers]
          real_answers = lesson.questions.map(&:answer)
          passed = false
          if user_answers == real_answers
            user.cookies += 1
            user.lessons << lesson.id
            course = lesson.course
            course_lessons = course.lessons.map(&:id)
            if course_lessons.all? { |l| user.lessons.include?(l) }
              user.courses << course.id
            end
            user.save
            passed = true
          end
          body Oj.dump({ passed: passed })
          content_type :json
          status :ok
        end

        # JSON-схема тела запроса
        REQUEST_BODY_SCHEMA = {
          type: :object,
          properties: {
            lesson_id: {
              type: :string
            },
            user_id: {
              type: :string
            },
            answers: {
              type: :array,
              items: {
                type: :string
              }
            }
          },
          required: %i[
            lesson_id
            user_id
            answers
          ]
        }.freeze

        # JSON-схема тела ответа
        RESPONSE_BODY_SCHEMA = {
          type: :object,
          properties: {
            passed: {
              type: :boolean
            }
          },
          required: %i[
            passed
          ]
        }.freeze
      end
    end
  end
end
