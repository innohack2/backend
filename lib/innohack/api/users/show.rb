# frozen_string_literal: true

module Innohack
  module API
    # Пространство методов REST API, предоставляющих доступ к данным
    # пользователей
    module Users
      # Пространство метода REST API, возвращающего информацию о пользователе
      module Show
        # Возвращает информацию о пользователе
        # @status
        #   200 OK
        # @response_body [JSON]
        #   информация о пользователе, структура которой соответствует
        #   JSON-схеме {RESPONSE_BODY_SCHEMA}
        Controller.get '/users/:id' do |id|
          user = Innohack::Models::User.find(id)
          data = {
            role: user.role,
            progress: "#{user.courses.count}/#{Innohack::Models::Course.count}",
            cookies: user.cookies
          }
          body Oj.dump(data)
          content_type :json
          status :ok
        end

        # JSON-схема тела ответа
        RESPONSE_BODY_SCHEMA = {
          type: :object,
          properties: {
            role: {
              type: :string,
              enum: ['backend']
            },
            progress: {
              type: :string,
              pattern: '\d+/\d+'
            },
            cookies: {
              type: :integer
            }
          },
          required: %i[
            role
            progress
            cookies
          ]
        }.freeze
      end
    end
  end
end
