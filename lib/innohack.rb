# frozen_string_literal: true

# Корневое пространство имён проекта. Базовый модуль
module Innohack
  # Возвращает полный путь к корневой директории проекта
  # @return [String]
  #   полный путь к корневой директории проекта
  def self.root
    File.realpath("#{__dir__}/..")
  end

  # Подключает нужный модуль приложения
  # @param [String] m0dule
  #   модуль приложения
  def self.equip(m0dule)
    Dir["#{__dir__}/innohack/**/*.rb"].sort.each do |file|
      require file if file =~ /#{m0dule}.rb/
      require file if file =~ %r{lib/innohack/#{m0dule}/}
    end
  end
end
