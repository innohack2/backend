# frozen_string_literal: true

require_relative 'config/helper'

Innohack::API::Controller.run!
