# frozen_string_literal: true

RSpec.configure do |config|
  # Стандартные настройки rspec
  config.expect_with :rspec do |expectations|
    expectations.include_chain_clauses_in_custom_matcher_descriptions = true
  end
  config.mock_with :rspec do |mocks|
    mocks.verify_partial_doubles = true
  end
  config.shared_context_metadata_behavior = :apply_to_host_groups

  # 1. Запретить использование конструкций describe без префикса RSpec (на
  #    верхнем уровне).
  # 2. Отключить поддержку старого синтаксиса (should и should_not).
  # 3. Отключить использование конструкций stub, should_receive и
  #    should_not_receive.
  config.disable_monkey_patching!

  # Если какие-либо тесты помечены тегом :focus, то запустить только их
  config.filter_run_when_matching :focus
end

require_relative '../config/helper'
require_relative 'support/factories'
require_relative 'support/json_schema'
require_relative 'support/rack'
require_relative 'support/mongo'
require_relative 'helpers/innohack/api/controller/spec_helper'
