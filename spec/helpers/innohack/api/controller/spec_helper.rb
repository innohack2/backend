# frozen_string_literal: true

module Innohack
  module API
    class Controller
      # Вспомогательный модуль, подключаемый к тестам REST API контроллера
      module SpecHelper
        # Справочник с информацией для тестирования работы контроллера при
        # различных сценариях
        SCENARIO = {
          ok: {
            route: '/test_route',
            block: lambda do
              body 'test route'
              status 200
            end,
            body: 'test route',
            status: 200
          },
          json_schema_validation_error: {
            route: '/json_schema_validation_error',
            block: -> { JSON::Validator.validate!({ type: :object }, []) },
            error_class: JSON::Schema::ValidationError,
            error_message: 'The property \'#/\' of type array did not match ' \
                           'the following type: object',
            body: Controller::JSON_SCHEMA_VALIDATION_ERROR_MESSAGE,
            status: :unprocessable_entity
          },
          document_not_found: {
            route: '/document_not_found',
            block: -> { ::Innohack::Models::User.find(0) },
            error_class: Mongoid::Errors::DocumentNotFound,
            error_message: 'Document\(s\) not found for class ' \
                           'Innohack::Models::User ' \
                           'with id\(s\) 0.',
            status: :not_found
          },
          internal_server_error: {
            route: '/internal_server_error',
            block: -> { raise 'Internal Server Error' },
            error_class: RuntimeError,
            error_message: 'Internal Server Error',
            body: 'RuntimeError: Internal Server Error',
            status: :internal_server_error
          }
        }.freeze

        # Создаёт маршруты для тестирования работы контроллера
        def define_test_routes
          SCENARIO.each_key do |key|
            Controller.get(SCENARIO[key][:route], &SCENARIO[key][:block])
          end
        end

        # Возвращает HTTP-код ответа в зависимости от тестируемого сценария
        # @param [Symbol] scenario
        #   тестируемый сценарий
        # @return [Integer]
        #   HTTP-код ответа
        def response_status(scenario)
          SCENARIO[scenario][:status]
        end

        # Возвращает тело ответа в зависимости от тестируемого сценария
        # @param [Symbol] scenario
        #   тестируемый сценарий
        # @return [String]
        #   тело ответа
        def response_body(scenario)
          SCENARIO[scenario][:body]
        end

        # Возвращает маршрут запроса для тестирования работы контроллера в
        # зависимости от тестируемого сценария
        # @param [Symbol] scenario
        #   тестируемый сценарий
        def route(scenario)
          SCENARIO[scenario][:route]
        end

        # Возвращает класс ошибки в зависимости от тестируемого сценария
        # @param [Symbol] scenario
        #   тестируемый сценарий
        # @return [Class]
        #   класс ошибки
        def error_class(scenario)
          SCENARIO[scenario][:error_class]
        end

        # Возвращает сообщение об ошибке в зависимости от тестируемого сценария
        # @param [Symbol] scenario
        #   тестируемый сценарий
        # @return [String]
        #   сообщение об ошибке
        def error_message(scenario)
          SCENARIO[scenario][:error_message]
        end
      end
    end
  end
end
