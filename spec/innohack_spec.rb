# frozen_string_literal: true

RSpec.describe Innohack do
  subject { described_class }

  it { is_expected.to respond_to(:root) }

  describe '.root' do
    let(:path) { described_class.root }

    it 'returns the full path to the project root directory' do
      expect(File).to exist("#{path}/spec/innohack_spec.rb")
    end
  end
end
