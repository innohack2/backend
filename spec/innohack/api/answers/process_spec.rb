# frozen_string_literal: true

RSpec.describe Innohack::API::Answers::Process do
  describe 'POST /answers' do
    subject(:response) { post '/answers', body, headers }

    let(:body) { Oj.dump(params) }
    let(:headers) { { 'CONTENT_TYPE' => 'application/json' } }
    let(:schema) { described_class::RESPONSE_BODY_SCHEMA }
    let(:request) { response }

    context 'when the request body matches the JSON schema' do
      let(:params) do
        { lesson_id: lesson_id, user_id: user_id, answers: answers }
      end
      let(:lesson_id) { lesson.id.to_s }
      let(:lesson) { create(:lesson) }
      let(:user_id) { user.id.to_s }
      let(:user) { create(:user) }
      let(:answers) { [] }

      context 'when there is a lesson and a user with the specified ids' do
        context 'when user has passed incorrect answers' do
          let(:answers) { lesson.questions.map { |q| q.variants.first } }

          it { is_expected.to respond_with(:ok) }

          it { is_expected.to respond_with_json_content_type }

          it { is_expected.to respond_with_proper_body(schema) }

          it 'returns info about the failed passing of the test' do
            expect(Oj.load(response.body)[:passed]).to be false
          end

          # rubocop:disable Lint/AmbiguousBlockAssociation
          it 'does not add cookies to the user' do
            expect { request }.not_to change { user.reload.cookies }
          end

          it 'does not add lesson to the lessons that user has passed' do
            expect { request }.not_to change { user.reload.lessons }
          end
          # rubocop:enable Lint/AmbiguousBlockAssociation
        end

        context 'when user has passed correct answers' do
          let(:answers) { lesson.questions.map(&:answer) }

          it { is_expected.to respond_with(:ok) }

          it { is_expected.to respond_with_json_content_type }

          it { is_expected.to respond_with_proper_body(schema) }

          it 'returns info about the successful passing of the test' do
            expect(Oj.load(response.body)[:passed]).to be true
          end

          it 'adds cookies to the user' do
            expect { request }.to change { user.reload.cookies }.by(1)
          end

          it 'adds lesson to the lessons that user has passed' do
            expect { request }
              .to change { user.reload.lessons }.to([lesson.id])
          end
        end
      end

      context 'when there is no lesson with the specified id' do
        let(:lesson_id) { 'non-existent' }

        it { is_expected.to respond_with(:not_found) }
      end

      context 'when there is no user with the specified id' do
        let(:user_id) { 'non-existent' }

        it { is_expected.to respond_with(:not_found) }
      end
    end

    context 'when the request body does not match the JSON schema' do
      let(:params) { { incorrect: true } }

      it { is_expected.to respond_with(:unprocessable_entity) }
    end
  end
end
