# frozen_string_literal: true

RSpec.describe Innohack::API::Courses::Index do
  describe 'GET /courses?user_id=id' do
    subject(:response) { get "/courses?user_id=#{user_id}" }

    before do
      user = create(:user)
      create(:course, roles: [])
      create(:course, roles: [user.role])
      passed = create(:course, :with_lessons, roles: [user.role])
      passed.lessons.each { |lesson| user.lessons << lesson.id }
      user.courses << passed.id
      user.save
    end

    let(:user_id) { Innohack::Models::User.first.id }
    let(:schema) { described_class::RESPONSE_BODY_SCHEMA }
    let(:list) { Oj.load(response.body) }

    it { is_expected.to respond_with(:ok) }

    it { is_expected.to respond_with_json_content_type }

    it { is_expected.to respond_with_proper_body(schema) }

    context 'when there is a general course in the list of courses' do
      it 'returns the general course' do
        expect(list[0][:type]).to eql('general')
      end
    end

    context 'when there is a user course in the list of courses' do
      it 'returns the user course' do
        expect(list[1][:type]).to eql('user')
      end
    end

    context 'when there is a course that user has passed' do
      let(:count) { Innohack::Models::Course.find(list[2][:id]).lessons.count }

      it 'returns the passed course' do
        expect(list[2][:type]).to eql('passed')
      end

      it 'returns full progress of the course' do
        expect(list[2][:progress]).to eql("#{count}/#{count}")
      end
    end
  end
end
