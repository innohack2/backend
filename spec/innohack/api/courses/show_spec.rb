# frozen_string_literal: true

RSpec.describe Innohack::API::Courses::Show do
  describe 'GET /courses/:id?user_id=id' do
    subject(:response) { get "/courses/#{id}?user_id=#{user_id}" }

    let(:schema) { described_class::RESPONSE_BODY_SCHEMA }
    let(:id) { course.id }
    let!(:course) { create(:course, :with_lessons) }
    let(:user_id) { user.id }
    let!(:user) { create(:user) }

    context 'when there is a course with the specified id' do
      context 'when there is a user with the specified id' do
        it { is_expected.to respond_with(:ok) }

        it { is_expected.to respond_with_json_content_type }

        it { is_expected.to respond_with_proper_body(schema) }

        context 'when user has passed some lessons' do
          before do
            user = Innohack::Models::User.first
            course = Innohack::Models::Course.first
            lesson = course.lessons.to_a[1]
            user.lessons << lesson.id
            user.save
          end

          let(:lessons) { Oj.load(response.body)[:lessons] }

          it 'returns info about this fact' do
            expect(lessons[1][:passed]).to be true
          end
        end
      end
    end

    context 'when there is no course with the specified id' do
      let(:id) { 'non-existent' }

      it { is_expected.to respond_with(:not_found) }
    end

    context 'when there is no user with the specified id' do
      let(:user_id) { 'non-existent' }

      it { is_expected.to respond_with(:not_found) }
    end
  end
end
