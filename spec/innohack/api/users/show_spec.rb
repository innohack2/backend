# frozen_string_literal: true

RSpec.describe Innohack::API::Users::Show do
  describe 'GET /users/:id' do
    subject(:response) { get "/users/#{id}" }

    let(:schema) { described_class::RESPONSE_BODY_SCHEMA }

    context 'when there is a user with the specified id' do
      let(:id) { create(:user).id }

      it { is_expected.to respond_with(:ok) }

      it { is_expected.to respond_with_json_content_type }

      it { is_expected.to respond_with_proper_body(schema) }
    end

    context 'when there is no user with the specified id' do
      let(:id) { 'non-existent' }

      it { is_expected.to respond_with(:not_found) }
    end
  end
end
