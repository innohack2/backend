# frozen_string_literal: true

RSpec.shared_context 'when testing logging with level' do |severity_level|
  around do |example|
    level = LOG.level
    LOG.level = severity_level
    example.run
    LOG.level = level
  end
end

RSpec.shared_examples 'error handler' do
  it { is_expected.to respond_with(response_status(scenario)) }

  context 'when the log severity level is ERROR or lower' do
    include_context 'when testing logging with level', :error

    it 'logs the error class' do
      expect { controller }
        .to output(/#{error_class(scenario)}/).to_stdout_from_any_process
    end

    it 'logs the error message' do
      expect { controller }
        .to output(/#{error_message(scenario)}/).to_stdout_from_any_process
    end

    it 'logs the place where the error occurred' do
      expect { controller }
        .to output(/#{__FILE__.sub("#{Innohack.root}/", '')}/)
        .to_stdout_from_any_process
    end
  end
end

RSpec.describe Innohack::API::Controller do
  include described_class::SpecHelper

  subject(:response) { send(request_method, request_path, payload, headers) }

  # rubocop:disable RSpec/BeforeAfterAll
  before(:all) { define_test_routes }
  # rubocop:enable RSpec/BeforeAfterAll

  let(:request_method) { 'get' }
  let(:request_path) { route(scenario) }
  let(:scenario) { :ok }
  let(:payload) {}
  let(:headers) { {} }

  # Псевдоним, улучшающий читабельность
  alias_method :controller, :subject

  it 'receives requests and returns responses' do
    expect(controller).to respond_with(:ok)
  end

  context 'when the log severity level is INFO or lower' do
    include_context 'when testing logging with level', :info

    it 'logs request method' do
      expect { controller }
        .to output(/#{request_method.upcase}/).to_stdout_from_any_process
    end

    it 'logs request path' do
      expect { controller }
        .to output(/#{request_path}/).to_stdout_from_any_process
    end

    context 'when request contains parameters' do
      let(:payload) { { 'test' => 'true' } }

      it 'logs request parameters' do
        expect { controller }
          .to output(/#{payload}/).to_stdout_from_any_process
      end
    end

    context 'when request contains body' do
      let(:request_method) { 'post' }
      let(:payload) { Oj.dump({ test: true }) }
      let(:headers) { { 'CONTENT_TYPE' => 'application/json' } }

      it 'logs request body' do
        expect { controller }
          .to output(/#{payload}/).to_stdout_from_any_process
      end
    end

    it 'logs response body' do
      expect { controller }
        .to output(/#{response_body(scenario)}/).to_stdout_from_any_process
    end

    it 'logs response status' do
      expect { controller }
        .to output(/#{response_status(scenario)}/).to_stdout_from_any_process
    end
  end

  context 'when the log severity level is higher than INFO' do
    include_context 'when testing logging with level', :warn

    it 'doesn\'t log any request or response' do
      expect { controller }.not_to output.to_stdout_from_any_process
    end
  end

  context 'when an error occurs during request processing' do
    context 'when the error is `JSON::Schema::ValidationError`' do
      let(:scenario) { :json_schema_validation_error }

      include_examples 'error handler'

      it 'returns a JSON schema validation error message' do
        expect(response.body).to eql response_body(scenario)
      end
    end

    context 'when the error is `Mongoid::Errors::DocumentNotFound`' do
      let(:scenario) { :document_not_found }

      include_examples 'error handler'
    end

    context 'when the error is `Internal Server Error`' do
      let(:scenario) { :internal_server_error }

      include_examples 'error handler'

      it 'returns the error info' do
        expect(response.body).to eql response_body(scenario)
      end
    end

    context 'when the log severity level is higher than ERROR' do
      let(:scenario) { :internal_server_error }

      include_context 'when testing logging with level', :fatal

      it 'doesn\'t log any information about the error' do
        expect { controller }.not_to output.to_stdout_from_any_process
      end
    end
  end
end
