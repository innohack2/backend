# frozen_string_literal: true

RSpec.describe Innohack::API::Auth::Login do
  describe 'POST /login' do
    subject(:response) { post '/login', body, headers }

    let(:body) { Oj.dump(params) }
    let(:headers) { { 'CONTENT_TYPE' => 'application/json' } }
    let(:schema) { described_class::RESPONSE_BODY_SCHEMA }

    context 'when the request body matches the JSON schema' do
      context 'when there is a user with the passed parameters' do
        let(:params) { { name: user.name, role: user.role } }
        let(:user) { create(:user) }

        it { is_expected.to respond_with(:ok) }

        it { is_expected.to respond_with_json_content_type }

        it { is_expected.to respond_with_proper_body(schema) }

        it 'returns the user id' do
          expect(Oj.load(response.body)).to eql(user.id.to_s)
        end
      end

      context 'when there is no user with the passed parameters' do
        let(:params) { attributes_for(:user).slice(:name, :role) }
        let(:user) { Innohack::Models::User.where(params).first }

        it { is_expected.to respond_with(:ok) }

        it { is_expected.to respond_with_json_content_type }

        it { is_expected.to respond_with_proper_body(schema) }

        it 'creates a user with the passed parameters' do
          expect(Oj.load(response.body)).to eql(user.id.to_s)
        end
      end
    end

    context 'when the request body does not match the JSON schema' do
      let(:params) { { incorrect: true } }

      it { is_expected.to respond_with(:unprocessable_entity) }
    end
  end
end
