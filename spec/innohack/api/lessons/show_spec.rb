# frozen_string_literal: true

RSpec.describe Innohack::API::Lessons::Show do
  describe 'GET /lessons/:id' do
    subject(:response) { get "/lessons/#{id}" }

    let(:id) { create(:lesson).id }
    let(:schema) { described_class::RESPONSE_BODY_SCHEMA }

    context 'when there is a lesson with the specified id' do
      it { is_expected.to respond_with(:ok) }

      it { is_expected.to respond_with_json_content_type }

      it { is_expected.to respond_with_proper_body(schema) }
    end

    context 'when there is no lesson with the specified id' do
      let(:id) { 'non-existent' }

      it { is_expected.to respond_with(:not_found) }
    end
  end
end
