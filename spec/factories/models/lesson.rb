# frozen_string_literal: true

FactoryBot.define do
  factory :lesson, class: 'Innohack::Models::Lesson' do
    to_create(&:save)

    name      { Faker::Lorem.sentence }
    content   { Faker::Lorem.paragraph }
    questions { create_list :question, 3, lesson: self }
    course
  end
end
