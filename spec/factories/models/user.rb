# frozen_string_literal: true

FactoryBot.define do
  factory :user, class: 'Innohack::Models::User' do
    to_create(&:save)

    name    { Faker::Name.name }
    role    { 'backend' }
    courses { [] }
    lessons { [] }
    cookies { Faker::Number.number }
  end
end
