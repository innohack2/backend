# frozen_string_literal: true

FactoryBot.define do
  factory :course, class: 'Innohack::Models::Course' do
    to_create(&:save)

    name        { Faker::Lorem.sentence }
    description { Faker::Lorem.paragraph }
    roles       { [] }

    trait :with_lessons do
      after :create do |course|
        create_list(:lesson, 3, course: course)
      end
    end
  end
end
