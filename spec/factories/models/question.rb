# frozen_string_literal: true

FactoryBot.define do
  factory :question, class: 'Innohack::Models::Question' do
    to_create(&:save)

    real_answer = Faker::Lorem.word

    title    { Faker::Lorem.sentence }
    variants { [Faker::Lorem.word, real_answer, Faker::Lorem.word] }
    answer   { real_answer }
    lesson
  end
end
