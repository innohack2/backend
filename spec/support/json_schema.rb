# frozen_string_literal: true

require 'json-schema'

# Определение проверок объектов на соответствие JSON-схеме

RSpec::Matchers.define :match_json_schema do |schema|
  match { |object| JSON::Validator.validate(schema, object) }

  description { "match the JSON schema #{schema}" }
end

RSpec::Matchers.define :respond_with_proper_body do |schema|
  match { |response| JSON::Validator.validate(schema, response.body) }

  description { "respond with body that matches the JSON schema #{schema}" }
end
