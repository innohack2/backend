# frozen_string_literal: true

# Поддержка тестирования mongo

require 'mongoid-rspec'
require 'database_cleaner-mongoid'

RSpec.configure do |config|
  config.include Mongoid::Matchers

  config.before(:suite) do
    DatabaseCleaner[:mongoid].strategy = :deletion
  end

  config.around do |example|
    DatabaseCleaner.cleaning do
      example.run
    end
  end
end
