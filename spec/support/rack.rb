# frozen_string_literal: true

# Поддержка тестирования контроллера Sinatra

require 'rack/test'

# Модуль, содержащий методы для поддержки тестирования контроллера Sinatra
module RackSupport
  include Rack::Test::Methods

  # Тестируемый REST-контроллер
  def app
    Innohack::API::Controller
  end
end

RSpec.configure do |config|
  config.include RackSupport
end

# Определение проверки HTTP-кода, возвращаемого в ответе
RSpec::Matchers.define(:respond_with) do |status|
  match { |response| response.status == code(status) }

  description { "respond with `#{code_with_message(code(status))}`" }

  failure_message do |response|
    "expected: #{code_with_message(code(status))}\n" \
    "     got: #{code_with_message(response.status)}"
  end

  # Возвращает числовое представление (код) статуса по его символьному названию
  # @param [Symbol] status
  #   символьное название статуса
  # @return [Integer]
  #   код статуса
  def code(status)
    Rack::Utils::SYMBOL_TO_STATUS_CODE[status]
  end

  # Возвращает код статуса с соответствующим сообщением
  # @param [Integer]
  #   код статуса
  # @return [String]
  #   код с сообщением
  def code_with_message(code)
    "#{code} #{Rack::Utils::HTTP_STATUS_CODES[code]}"
  end
end

# Определение проверки значения `application/json` в HTTP заголовке
# `Content-Type`
RSpec::Matchers.define(:respond_with_json_content_type) do
  match { |response| response.headers['Content-Type'] == 'application/json' }

  description { 'respond with `Content-Type: application/json`' }

  failure_message do |response|
    "expected: application/json\n" \
    "     got: #{response.headers['Content-Type']}"
  end
end
